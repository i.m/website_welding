from app import app, db
from app.models import User, Project, Keyword

db.create_all()

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Post': Project, 'Keyword': Keyword}


if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    app.run(host='0.0.0.0')
# [END gae_flex_quickstart]
