import os
from app import app, db
from app.models import User, Project, Keyword
from werkzeug.security import generate_password_hash
basedir = os.path.abspath(os.path.dirname(__file__))

db.create_all()
email = os.environ.get('SECRET_EMAIL') or 'test@email.com'
try:
  user = User.query.filter_by(email=email).first()
  print(email, user)
  if user is not None:
    db.session.delete(user)
    print(User.query.all())
  u = User(email="test@email.com")
  u.urole='Admin'
except:
  u = User(email="test@email.com")
  u.urole='Admin'
pwd = generate_password_hash(os.environ.get('SECRET_PASSWORD') or 'test')
u.set_password(pwd)
db.session.add(u)
db.session.commit()
