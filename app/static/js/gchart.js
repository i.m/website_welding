$('.gchart').each(function (index) {
  // Load the Visualization API and the corechart package.
  google.charts.load('current', {
    'packages': ['corechart', 'calendar']
  });

  function fuckThis() {
    var data = document.getElementById('waterChart');
    var fuckThisToo = data.dataset.value;
    return fuckThisToo;
  }
  var val = fuckThis();

  function fuckThis2() {
    var data = document.getElementById('humidityChart');
    var fuckThisToo = data.dataset.value;
    return fuckThisToo;
  }
  var val2 = fuckThis2();

  // Callback to run when the Google Visualization API is loaded.
  google.charts.setOnLoadCallback(drawHumidityChart);
  google.charts.setOnLoadCallback(drawWaterChart);

  // Callbacks that creates and populates a data table,
  // instantiate the chart, passe in the data and
  // draw it.
  function drawHumidityChart() {
    var data = new google.visualization.DataTable();
    data.addColumn({
        type: 'number',
        id: 'Jours'
    });
    data.addColumn({
      type: 'number',
      id: 'Humidité'
    });
    var niqueTonPere = JSON.parse(val2);
    for (var i = 0; i < niqueTonPere.humidity.length; ++i)
    {
      var date = niqueTonPere.humidity[i].time.substr(14, 1)
      var valueWater = niqueTonPere.humidity[i].value
      console.log(valueWater, date)
      data.addRows([[parseInt(date), valueWater]]);
    }
    var options = {
      'title': 'Humidité du sol',
      hAxis: { title: 'Dizaine de minutes', titleTextStyle: { color: '#333' }, minValue: 0 },
      vAxis: { title: 'Humidité (%)', minValue: 0, maxValue:100 }
    };
    var chart = new google.visualization.AreaChart(document.getElementById('humidityChart'));
    chart.draw(data, options);
  }

  function drawWaterChart() {
    var dataTable = new google.visualization.DataTable();
    dataTable.addColumn({ type: 'date', id: 'Date' });
    dataTable.addColumn({ type: 'number', id: 'Nombre d\'arrosage' });
    var niqueTaMere = JSON.parse(val)
    rowList = []
    for (var i = 0; i < niqueTaMere.length; ++i)
    {
      var date = niqueTaMere[i].time.substr(0, 10).split('-');
      var quantity = niqueTaMere[i].quantity_cl;
      rowList.push([new Date(parseInt(date[0], 10), parseInt(date[1], 10) - 1, parseInt(date[2], 10)), quantity]);
      console.log(rowList[i])
    }
    dataTable.addRows(rowList);
    var chart = new google.visualization.Calendar(document.getElementById('waterChart'));
    var options = {
      title: "Quantité arrosée (cl)",
      height: 400,
      noDataPattern:
      {
        backgroundColor: '#000000',
        color: '#ffffff'
      },
      calendar: {
        daysOfWeek: 'DLMMJVS',
      }
    };
    chart.draw(dataTable, options);
  }
});