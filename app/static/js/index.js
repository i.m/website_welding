$(function () {
  var selectedClass = "";
  $(".filter").click(function () {
    selectedClass = $(this).attr("data-rel");
    $("#gallery").fadeTo(100, 0.1);
    $("#gallery div").not("." + selectedClass).fadeOut().removeClass('animation');
    setTimeout(function () {
      $("." + selectedClass).fadeIn().addClass('animation');
      $("#gallery").fadeTo(300, 1);
    }, 300);
  });
});


var coordinate = [45.689999, 5.915638];
L.mapbox.accessToken = 'pk.eyJ1IjoibWVzaHVzaGkiLCJhIjoiY2s4MGZnNHFqMDFhODN1bXdrOXE1c3BibiJ9._A0wPC_lpUfXxkNh9rrtNQ';
var map = L.mapbox.map('map')
  .setView(coordinate, 17)
  .addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v11'));
L.marker(coordinate, {
  icon: L.mapbox.marker.icon({
    'marker-color': '#f86767'
  }),
}).addTo(map)
