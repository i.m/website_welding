// $(document).on('change', '#browsebutton :file', function () {
//   var input = $(this),
//     numFiles = input.get(0).files ? input.get(0).files.length : 1,
//     label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
//   input.trigger('fileselect', [numFiles, label]);
// });

if (!String.prototype.isInList) {
  String.prototype.isInList = function (list) {
    let value = this.valueOf();
    for (let i = 0, l = list.length; i < l; i += 1) {
      if (value.endsWith(list[i])) return true;
    }
    return false;
  }
}


$(function () {
  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function () {
    var extension = [".jpg", ".jpeg", ".png", ".gif"];
    var result = []
    var input = $(this);
    var files = input.get(0).files;
    var isOk = true
    for (var i = 0; i < files.length && isOk; ++i) {
      var isOk = files[i].name.isInList(extension)
      if (!isOk) {
        input.trigger('fileselect', ["Erreur: le fichier", files[i].name, "n'est pas une image"]);
      }
      result.push(files[i].name)
    }
    if (isOk)
    {
      numFiles = input.get(0).files ? input.get(0).files.length : 1;
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    }
  });

  // We can watch for our custom `fileselect` event like this
  $(document).ready(function () {
    $(':file').on('fileselect', function (event, numFiles, label) {
      var input = $(this).parents('.input-group').find(':text'),
        log = numFiles > 1 ? numFiles + ' files selected' : label;
      if (input.length) {
        input.val(log);
      } else {
        if (log) alert(log);
      }
    });
  });
});