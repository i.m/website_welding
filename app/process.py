import json
import requests
import os
from flask import Flask, flash, request, redirect, url_for
from app import app


ALLOWED_EXTENSIONS = set(['pdf', 'png', 'jpg', 'jpeg', 'gif'])


def ajax_files(text):
    print(text)
    return "OK"


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
