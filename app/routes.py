import os
import urllib.request
from markupsafe import escape
from werkzeug.urls import url_parse
from werkzeug.utils import secure_filename
from flask import render_template, request, url_for, flash, redirect, jsonify, send_from_directory
from flask_login import current_user, login_user, logout_user, login_required
from app import app, db
from app.models import User, Project, Image, Keyword
from app.models import User, Keyword, Project, Image
from app.process import allowed_file
from app.forms import LoginForm, UploadForm, KeywordForm
from app.wrappers import user_required


@app.route('/')
def index():
    return render_template("index.html")
    # return 'Index Page'


@app.route('/home')
@app.route('/index')
def home():
    characteristics_list = [
        {
            'description': "Nos artisants sont qualifiés et utilisent les meilleurs techniques et outils pour réaliser leurs créations. En optant pour <i>La Belle Soudure & Co</i> vous optez pour le gage du savoir faire à la française.",
            'image': "processor",
            'short_description': "Savoir faire",
            'alt': "Savoir faire"
        },
        {
            'description': "Nos professionels ont les compétences pour travailler sur les différents bois et métaux, apportant ainsi la meilleure coupe des surfaces en fonction des idées travaillées.",
            'image': "sand-clock",
            'short_description': "Utilisation de materiaux authentiques",
            'alt': "Matériaux"
        },
        {
            'description': "Les matériaux utilisées viennent de France et en choisissant <em>La Belle Soudure & Co</em> vous faites travailler l'économie locale.",
            'image': "magic-wand",
            'short_description': "Qualité «Made in France»",
            'alt': "Made in France"
        },
        {
            'description': "",
            'image': "dashboard",
            'short_description': "Réalisation de vos idées",
            'alt': "Idées"
        }
    ]
    gallery_list = [
        {
            'src': "https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(73)",
            'section': ["Sea"]
        },
        {
            'src': "https://mdbootstrap.com/img/Photos/Others/images/86",
            'section': ["Sea", "Mountains"]
        },
        {
            'src': "https://mdbootstrap.com/img/Photos/Vertical/mountain2",
            'section': ["Mountains"]
        },
        {
            'src': "https://mdbootstrap.com/img/Photos/Vertical/mountain1",
            'section': ["Mountains"]
        },
        {
            'src': "https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35)",
            'section': ["Sea"]
        },
        {
            'src': "https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(18)",
            'section': ["Sea"]
        },
        {
            'src': "https://mdbootstrap.com/img/Photos/Vertical/mountain3",
            'section': ["Mountains"]
        }
    ]
    index_list = set([it for item in gallery_list for it in item['section']])
    projects = Project.query.all()
    keywords = Keyword.query.all()
    return render_template("home.html", characteristics=characteristics_list, gallery=gallery_list, gallery_keywords=index_list, projects=projects, keywords=keywords)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('admin'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid email or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('home')
        return redirect(next_page)
    return render_template('login.html', title='Se connecter', form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register')
@login_required
@user_required(role='Admin')
def administration():
    return render_template("administration.html", title="Ajouter d'utilisateurs")


@app.route('/test')
def test():
    project = Project.query.filter_by(id=1).first()
    return render_template('test.html', title=f"Test : {project.title}", project=project)


@app.route('/projects/add', methods=['GET', 'POST'])
@login_required
def add_project():
    # keywords = Keyword.query.all()
    form = UploadForm()
    form.tags.choices = [(k.id, k.name) for k in Keyword.query.all()]
    # form.keywords = [(key.id, key.name) for key in keywords]
    if form.validate_on_submit():
        # Creating project
        project = Project(title=form.title.data, body=form.body.data,
                  user_id=current_user.id)
        db.session.add(project)
        db.session.commit()
        # Adding images name to project
        files = request.files.getlist(form.files.name)
        for file_upload in files:
            filename = secure_filename(file_upload.filename)
            saved_filename = f"{app.config['UPLOAD_FOLDER']}/projectid_{project.id}_{filename}"
            file = os.path.join(saved_filename)
            file_upload.save(file)
            i = Image(image_name=f"projectid_{project.id}_{filename}")
            db.session.add(i)
            db.session.commit()
            project.images.append(i)
        # Adding keywords to project
        keyword_list = [ k for k in form.tags.raw_data]
        for keyword in keyword_list:
            k = Keyword.query.filter_by(id=keyword).first()
            if k and (k.name != "" or k.name != " "):
              project.keywords.append(k)
            else:
              return render_template("500.html"), 500
        # Creating new keywords and adding them to project
        for keyword in form.additional_tags.data:
          new_key = Keyword.query.filter_by(name=keyword).first()
          if not new_key:
            new_key = Keyword(name=keyword)
            db.session.add(new_key)
            db.session.commit()
          project.keywords.append(new_key)

        db.session.add(project)
        db.session.commit()
        return redirect(url_for('project_details', project_name=project.title, project_id=project.id))
    return render_template('upload.html', title="Ajout de projet", form=form)


@app.route('/projects/')
def projects():
    projects = Project.query.all()
    return render_template('projects.html', title="Liste de projets", projects=projects)


@app.route('/img/<int:image_id>')
def serve_image(image_id):
    src = Image.query.filter_by(id=image_id).first()
    return f"uploads/{src.image_name}"

@app.route('/projects/<int:project_id>/<string:project_name>')
def project_details(project_name, project_id):
    project = Project.query.filter_by(id=project_id).first()
    return render_template('project_details.html', title=f"{project.title}",project=project)

@app.route('/keywords', methods=['GET', 'POST'])
@login_required
@user_required(role='Admin')
def manage_keywords():
    keywords = Keyword.query.all()
    form = KeywordForm()
    form.tags.choices = [(k.id, k.name) for k in Keyword.query.all()]
    return render_template('manage_keywords.html', title="Gestion des mots-clés", keywords=keywords, form=form)

@app.route('/delete/<int:project_id>', methods=['POST'])
@login_required
@user_required(role='Admin')
def delete_project(project_id):
    p = Project.query.filter_by(id=project_id).first()
    images = p.images
    for image in images:
        file_upload = f"{app.config['UPLOAD_FOLDER']}/{image.image_name}"
        db.session.delete(image)
        file = os.path.join(file_upload)
        os.remove(file)
    db.session.delete(p)
    db.session.commit()
    return redirect(url_for('projects'))

@app.route('/delete/keyword/<int:keyword_id>', methods=['POST'])
@login_required
@user_required(role='Admin')
def delete_keyword(keyword_id):
    k = Keyword.query.filter_by(id=keyword_id).first()
    db.session.delete(k)
    db.session.commit()
    return redirect(url_for('manage_keywords'))

if __name__ == "__main__":
    app.run(host="0.0.0.0")