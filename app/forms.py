from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms import StringField, PasswordField, BooleanField, SubmitField, SelectField, SelectMultipleField, TextAreaField, widgets
from wtforms.widgets import html_params, HTMLString
from wtforms.widgets.core import *
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from app.models import User, Keyword


class AdditionalTagsField(StringField):
    def __init__(self, choices=[], **args):
        super(AdditionalTagsField, self).__init__(**args)
        self.choices = [c[1] for c in choices]
    def pre_validate(self, form):
        intersection = [inter for inter in self.raw_data if inter in self.choices]
        if intersection:
            raise ValueError(self.gettext(f'Le(s) mot(s)-clé(s) {", ".join(intersection)} ont déjà créé.'))

    def process_formdata(self, valuelist):
        if valuelist:
            self.data = [x.strip() for keyword_list in valuelist for x in keyword_list.split(',') ]
        else:
            self.data = []

class Select2MultipleField(SelectMultipleField):

    def pre_validate(self, form):
        # Prevent "not a valid choice" error
        pass

    def process_formdata(self, valuelist):
        if valuelist:
            self.data = ",".join(valuelist)
        else:
            self.data = ""


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Mot de passe', validators=[DataRequired()])
    remember_me = BooleanField('Se rappeler de moi')
    submit = SubmitField('Connexion')


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')


class KeywordForm(FlaskForm):
    tags = Select2MultipleField("Mots-clés", [],
                                description=u"Description Tags",
                                render_kw={"multiple": "multiple", "data-tags": "1"})
    additional_tags = AdditionalTagsField(description="Ajouter des mots clés")
    submit = SubmitField('Valider')


class UploadForm(FlaskForm):
    title = StringField('Titre', validators=[DataRequired()])
    body = TextAreaField('Description', validators=[DataRequired()])
    # keywords = SelectMultipleField(label='Catégories', choices=[(
    #     'cpp', 'C++'), ('py', 'Python'), ('text', 'Plain Text')], validators=[DataRequired()])
    # new_keyword = StringField('Nouvelle catégorie', validators=[DataRequired()])
    # files = MultipleFileField('Fichiers à téléverser')
    files = FileField(
        'Fichiers à téléverser',
        validators=[
            FileRequired(),
            FileAllowed(['jpg', 'jpeg', 'png'], 'Images only!')
    ])
    tags = Select2MultipleField("Mots-clés", [],
                                description=u"Description Tags",
                                render_kw={"multiple": "multiple", "data-tags": "1"})
    additional_tags = AdditionalTagsField(description="Mot clé non trouvé ?")
    submit = SubmitField('Valider')
