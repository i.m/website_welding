from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship, backref
from flask_migrate import Migrate
from flask_login import LoginManager


# Flask common settings
# def create_app(config_class=Config):
app = Flask(__name__)
app.config.from_object(Config)
# Image importation settings
UPLOAD_FOLDER = 'app/static/uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 64 * 1024 * 1024
# Flask-SQLAlchemy settings
db = SQLAlchemy(app)
migrate = Migrate(app, db)
# Flask-login settings
login = LoginManager(app)
login.login_view = 'login'

# return app
if __name__ == '__main__':
    app.run(host="0.0.0.0")



@app.template_filter('safe_string')
def safe_string(s):
    return s.replace(' ', '_')


from app import routes, models, errors, wrappers
