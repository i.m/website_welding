from functools import wraps
from flask_login import current_user
from app import login

def user_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):

            if not current_user.is_authenticated:
               return login.unauthorized()

            if ((current_user.get_urole() != role) and (role != "ANY")):
                return login.unauthorized()
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper
