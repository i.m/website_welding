from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime
from app import db, login


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    urole = db.Column(db.String(80))

    def __repr__(self):
        return f'<User {self.email}>'

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
    
    def get_urole(self):
        return self.urole


project = db.Table('projet',
    db.Column('project_id', db.Integer, db.ForeignKey('project.id')),
    db.Column('keyword_id', db.Integer, db.ForeignKey('keyword.id')),
    db.Column('image_id', db.Integer, db.ForeignKey('image.id'))
)


class Image(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    image_name = db.Column(db.String(50))
    project = db.relationship('Project', secondary=project, backref=db.backref('images', lazy='dynamic'))

    def __repr__(self):
        return f'<Image | name : {self.image_name}>'


class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(50))
    body = db.Column(db.Text)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return f'<Project_{self.id}_{self.title}>'


class Keyword(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(25), nullable=False)
    project = db.relationship('Project', secondary=project, backref=db.backref('keywords', lazy='dynamic'))

    def __repr__(self):
        return f'<Keyword | nom : {self.name}>'

    def safe_string(self):
        return f"{self.name.replace(' ', '_')}"


@login.user_loader
def load_user(id):
    return User.query.get(int(id))
